package com.publicis.cards.validators;

import com.publicis.cards.CardApplicationBaseTest;
import com.publicis.cards.dto.CreditCardDTO;
import com.publicis.cards.model.CreditCard;
import org.assertj.core.api.Assertions;
import org.javamoney.moneta.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

public class CreditCardValidatorTest extends CardApplicationBaseTest {

    @Inject
    private Validator validator;

    //TODO Added unit tests for basic use cases, Could add more for all the edge cases

    @Test
    public void shouldFailValidationForInvalidCreditCardNumber() {
        // given
        CreditCardDTO creditCard = CreditCardDTO.builder().creditCardNumber("5592925105282888").build();

        // when
        Set<ConstraintViolation<CreditCardDTO>> violations = validator.validate(creditCard);

        // then
        Assertions.assertThat(violations)
                .hasSize(3)
                .extracting("message").containsOnly("Invalid Credit Card number","must not be null");
    }

    @Test
    public void shouldPassValidationForValidCreditCardNumber() {
        // given
        CreditCardDTO creditCard = CreditCardDTO.builder()
                .creditCardNumber("4929185800037136")
                .name("Anil")
                .limit(Money.parse("GBP 20000.00"))
                .build();

        // when
        Set<ConstraintViolation<CreditCardDTO>> violations = validator.validate(creditCard);

        // then
        Assertions.assertThat(violations)
                .hasSize(0);
    }

}