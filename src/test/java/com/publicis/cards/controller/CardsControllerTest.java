package com.publicis.cards.controller;

import com.jayway.restassured.response.Response;
import com.publicis.cards.dto.CreditCardDTO;
import com.publicis.cards.CardApplicationBaseTest;
import org.javamoney.moneta.Money;
import org.junit.Test;


public class CardsControllerTest extends CardApplicationBaseTest {
    private final String CARDS_API_PATH = "/api/cards";


    @Test
    public void shouldReturn400IfInvalidCardDetailsAreProvided() {
        //given
        CreditCardDTO invalidCardDTO = CreditCardDTO.builder().build();
        //when
        whenHttpRequest()
                .body(invalidCardDTO)
                .post(CARDS_API_PATH)
                .then()
                .statusCode(400);
    }

    @Test
    public void shouldSuccessfullyAddNewCardWhenCardDetailsAreValid() {
        //given
        CreditCardDTO validCardDTO = CreditCardDTO.builder()
                .name("Anil")
                .creditCardNumber("4539511914173503")
                .limit(Money.of(200, "GBP"))
                .build();
        //when
        Response response = whenHttpRequest()
                .body(validCardDTO)
                .post(CARDS_API_PATH);
        response
                .then()
                .statusCode(200);
    }

    @Test
    public void shouldGetAllCards() {
        //when & then
        whenHttpRequest()
                .get(CARDS_API_PATH)
                .then()
                .statusCode(200);

    }

}
