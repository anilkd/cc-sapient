package com.publicis.cards;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.specification.RequestSpecification;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.zalando.jackson.datatype.money.MoneyModule;

import static com.jayway.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CardApplicationBaseTest {
    private static final RestAssuredConfig REST_ASSURED_CONFIG = initRestAssuredConfig();
    @LocalServerPort
    private int containerPort;

    private static RestAssuredConfig initRestAssuredConfig() {
        RestAssured.defaultParser = Parser.JSON;
        return RestAssuredConfig.config()
                .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((aClass, s) -> {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.registerModule(new MoneyModule().withQuotedDecimalNumbers());
                    objectMapper.registerModule(new JavaTimeModule());
                    return objectMapper;
                }));
    }

    @Test
    public void contextLoads() {
    }

    protected RequestSpecification whenHttpRequest() {
        return given().config(REST_ASSURED_CONFIG)
                .port(containerPort)
                .header("RequestId", "request-id-foo")
                .contentType(ContentType.JSON);
    }

}
