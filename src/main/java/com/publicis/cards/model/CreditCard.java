package com.publicis.cards.model;

import com.publicis.cards.validators.ValidCreditCard;
import lombok.Builder;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true)
public class CreditCard {
    @ValidCreditCard
    private final String number;

    @NotNull
    private String id;

    @NotNull
    private String name;
    //TODO custom money validation
    @NotNull
    private Money balance;

    //TODO custom money validation
    private Money limit;
}
