package com.publicis.cards.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Customer {
    private final String id;
    private final String name;
    private final CreditCard creditCard;
}
