package com.publicis.cards.service;

import com.publicis.cards.dto.CreditCardDTO;
import com.publicis.cards.model.CreditCard;
import com.publicis.cards.repository.CardRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DefaultCardService implements CardService {

    private final CardRepository cardRepository;

    @Inject
    public DefaultCardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public CreditCardDTO addCard(CreditCardDTO creditCard) {
        CreditCard newCard = cardRepository.add(convertDTOToModel(creditCard));
        return convertModelToDTO(newCard);
    }

    @Override
    public List<CreditCardDTO> getAllCards() {
        return cardRepository.findAll()
                .stream()
                .map(this::convertModelToDTO)
                .collect(Collectors.toList());
    }

    private CreditCardDTO convertModelToDTO(CreditCard creditCard) {
        return CreditCardDTO.builder()
                .creditCardNumber(creditCard.getNumber())
                .balance(creditCard.getBalance())
                .limit(creditCard.getLimit())
                .name(creditCard.getName())
                .id(creditCard.getId())
                .build();

    }

    private CreditCard convertDTOToModel(CreditCardDTO dto) {
        return CreditCard.builder()
                .number(dto.getCreditCardNumber())
                .balance(dto.getBalance())
                .limit(dto.getLimit())
                .name(dto.getName())
                .id(UUID.randomUUID().toString())
                .build();
    }
}
