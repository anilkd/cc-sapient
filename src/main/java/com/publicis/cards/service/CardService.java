package com.publicis.cards.service;

import com.publicis.cards.dto.CreditCardDTO;

import java.util.List;

public interface CardService {

    CreditCardDTO addCard(CreditCardDTO creditCard);

    List<CreditCardDTO> getAllCards();
}
