package com.publicis.cards.controller;

import com.publicis.cards.dto.CreditCardDTO;
import com.publicis.cards.service.CardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/cards")
@Slf4j
public class CardsController {
    private final CardService cardService;

    @Inject
    public CardsController(CardService cardService) {
        this.cardService = cardService;
    }


    @PostMapping
    public CreditCardDTO addCard(@Valid @RequestBody CreditCardDTO creditCard) {
        log.info("saving creditCard");
        return cardService.addCard(creditCard);
    }

    @GetMapping
    public List<CreditCardDTO> getAllCards() {
        log.info("getting all creditCard");
        return cardService.getAllCards();
    }

}
