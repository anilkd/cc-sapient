package com.publicis.cards;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ResourceLoader;
import org.zalando.jackson.datatype.money.MoneyModule;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

@SpringBootApplication
public class CardApplication {

    public static void main(String[] args) {
        SpringApplication.run(CardApplication.class, args);
    }

    @Bean
    @Primary
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new MoneyModule().withQuotedDecimalNumbers());
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSSZ"));

        return objectMapper;
    }


    @Bean
    public Firestore getFireStore(GoogleCredentials credentials) {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .setProjectId("badminton-service02")
                .build();

        FirebaseApp.initializeApp(options);

        return FirestoreClient.getFirestore();
    }

    @Bean
    public GoogleCredentials googleCredentials(ResourceLoader resourceLoader) throws IOException {
        InputStream serviceAccount = new FileInputStream(resourceLoader.getResource("classpath:firestore.json").getFile());
        return GoogleCredentials.fromStream(serviceAccount);
    }

}
