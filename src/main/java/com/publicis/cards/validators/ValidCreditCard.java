package com.publicis.cards.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = CreditCardValidator.class)
public @interface ValidCreditCard {

    String message() default "Invalid Credit Card number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int max() default 19;

    // int min() default 12;
}