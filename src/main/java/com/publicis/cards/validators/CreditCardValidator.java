package com.publicis.cards.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CreditCardValidator implements ConstraintValidator<ValidCreditCard, String> {
    private int maxLength;

    private boolean isValid(String ccNumber) {
        int sum = 0;
        boolean alternate = false;
        if (ccNumber == null || !(ccNumber.length() <= maxLength)) {
            return false;
        }
        for (int i = ccNumber.length() - 1; i >= 0; i--) {

            int n = Integer.parseInt(ccNumber.substring(i, i + 1));
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n -= 9;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }

    @Override
    public void initialize(ValidCreditCard constraintAnnotation) {
        this.maxLength = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isValid(value);
    }
}
