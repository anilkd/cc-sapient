package com.publicis.cards.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.publicis.cards.model.CreditCard;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CardRepository {

    private final Firestore firestore;
    private final ObjectMapper objectMapper;

    @Inject
    public CardRepository(Firestore firestore, ObjectMapper objectMapper) {
        this.firestore = firestore;
        this.objectMapper = objectMapper;
    }

    public CreditCard add(CreditCard creditCard) {
        DocumentReference tournamentRef = firestore.collection("credit-cards").document(creditCard.getId());
        Map map = objectMapper.convertValue(creditCard, Map.class);
        ApiFuture<WriteResult> apiFuture = tournamentRef.set(map);
        try {
            Timestamp updateTime = apiFuture.get().getUpdateTime();
            log.info("Added new card with id {}", creditCard.getId());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        return creditCard;
    }

    public List<CreditCard> findAll() {
        ApiFuture<QuerySnapshot> querySnapshotApiFuture = firestore.collection("credit-cards").get();
        try {
            return querySnapshotApiFuture.get().getDocuments().stream()
                    .map(qs -> objectMapper.convertValue(qs.getData(), CreditCard.class))
                    .collect(Collectors.toList());
        } catch (InterruptedException | ExecutionException e) {
            //TODO could define application level exceptions
            throw new RuntimeException(e);
        }
    }
}
