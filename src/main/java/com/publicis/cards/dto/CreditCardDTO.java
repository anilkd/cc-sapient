package com.publicis.cards.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.publicis.cards.validators.ValidCreditCard;
import lombok.Builder;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.validation.constraints.NotNull;

@Value
@Builder(toBuilder = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditCardDTO {
    @NotNull
    private final String name;
    @ValidCreditCard
    private final String creditCardNumber;

    private final Money balance;

    @NotNull
    private final Money limit;

    private final String id;
}
