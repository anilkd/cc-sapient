package com.sathwik.badmintonservice.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Player {

    private final String name;
    private final String phone;
    private final String email;
}
