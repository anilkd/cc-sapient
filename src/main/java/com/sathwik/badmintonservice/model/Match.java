package com.sathwik.badmintonservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Match {
    private String id;
    private String teamOne;
    private String teamTwo;
    private String tournamentId;
//    private boolean matchFinished;
//@DateTimeFormat(iso = ISO_LOCAL_DATE_TIME)

@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss")
    private final LocalDateTime matchStartDateTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss")
    private final LocalDateTime matchEndDateTime;
    private int court;
    private int numberOfSets;
    private int setPoints;
    private List<Set> sets;

    @JsonProperty("matchFinished")
    public boolean isMatchFinished() {
        Map<String, List<Set>> stringListMap = sets.stream().collect(Collectors.groupingBy(Set::getWinnerId));
        return stringListMap.entrySet().stream().anyMatch(stringListEntry -> stringListEntry.getValue().size() > (numberOfSets / 2));
    }
}
