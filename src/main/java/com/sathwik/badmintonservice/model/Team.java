package com.sathwik.badmintonservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Data
@Builder
public class Team {
    private  String id;
    private  String name;
    private  List<Player> players;
    private  String description;
}
