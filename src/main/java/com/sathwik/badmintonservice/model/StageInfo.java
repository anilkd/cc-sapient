package com.sathwik.badmintonservice.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class StageInfo {
    private final String name;
    private final int teamsPerGroup;
    private final int numberOfSets;
    private final int setPoints;
    private final int topNTeamsFromGroup;
    private final String prevStageId;
}
