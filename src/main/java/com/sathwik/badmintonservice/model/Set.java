package com.sathwik.badmintonservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Set {

    private final String teamOneId;
    private final String teamTwoId;
    private final int teamOnePoints;
    private final int teamTwoPoints;

    public String getWinnerId(){
        return teamOnePoints > teamTwoPoints ? teamOneId: teamTwoId;
    }

    public String getSecondPlaceId(){
        return teamOnePoints > teamTwoPoints ? teamTwoId: teamOneId;
    }

    public int getWinningScoreDiff(){
        return Math.abs(teamOnePoints-teamTwoPoints);
    }

    public boolean isSetFinished(int setPoints){
        return ((teamOnePoints >= setPoints || teamTwoPoints >= setPoints) && getWinningScoreDiff() >= 2);
    }
}
