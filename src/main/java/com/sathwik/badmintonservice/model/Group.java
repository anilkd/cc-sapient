package com.sathwik.badmintonservice.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Group {
    private final String name;
    private final List<String> teams;
    private final List<Match> matches;
    private final List<String> toppers;
}
