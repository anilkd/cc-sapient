package com.sathwik.badmintonservice.model;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Stage {
    private final String id;
    private final List<Group> groups;
    private final String name;
}
