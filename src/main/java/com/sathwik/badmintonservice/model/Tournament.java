package com.sathwik.badmintonservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.groupingBy;

@Data
@NoArgsConstructor
@Builder(toBuilder = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tournament {
    private String name;
    private String id;
    @Singular
    private List<Team> teams = Lists.newArrayList();
    private int initialTeamsPerGroup;
    @Singular
    private List<Stage> stages = Lists.newArrayList();
    private Map<String, List<Team>> teamsMap;

    public Tournament(String name, String id, List<Team> teams, int initialTeamsPerGroup, List<Stage> stages, Map<String, List<Team>> teamsMap) {
        this.name = name;
        this.id = id;
        this.teams=teams!=null?teams:Lists.newArrayList();
        this.initialTeamsPerGroup = initialTeamsPerGroup;
        this.stages=stages!=null?stages:Lists.newArrayList();
        this.teamsMap = teamsMap;
    }

    @JsonProperty
    public Map<String, List<Team>> getTeamsMap() {
        return Optional.ofNullable(teams)
                .map(teams1 -> teams1.stream()
                        .collect(groupingBy(Team::getId)))
                .orElseGet(HashMap::new);
    }

/*    public static  class TournamentBuilder {
        private List<Team> teams = Lists.newArrayList();
        private List<Stage> stages = Lists.newArrayList();

    }*/
}
