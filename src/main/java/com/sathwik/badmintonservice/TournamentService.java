package com.sathwik.badmintonservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Streams;
import com.sathwik.badmintonservice.combinators.Combinator;
import com.sathwik.badmintonservice.model.*;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Collections.reverseOrder;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

@Service
public class TournamentService {
    private final Firestore firestore;
    private final ObjectMapper objectMapper;

    @Inject
    public TournamentService(Firestore firestore, ObjectMapper objectMapper) {
        this.firestore = firestore;
        this.objectMapper = objectMapper;
    }

    public Tournament createTournament(Tournament tournament) throws ExecutionException, InterruptedException {
        String id = UUID.randomUUID().toString();
        Tournament tournamentModel = Tournament.builder()
                .id(id)
                .teams(tournament.getTeams())
                .stages(tournament.getStages())
                .name(tournament.getName())
                .initialTeamsPerGroup(tournament.getInitialTeamsPerGroup())
                .build();
        DocumentReference tournamentRef = firestore.collection("tournaments").document(id);
        Map map = objectMapper.convertValue(tournamentModel, Map.class);
        ApiFuture<WriteResult> apiFuture = tournamentRef.set(map);
        System.out.println("Tournament added : " + apiFuture.get().getUpdateTime());
        return tournamentModel;
    }

    public Tournament updateTournament(Tournament tournament) throws ExecutionException, InterruptedException {
        DocumentReference tournamentRef = firestore.collection("tournaments").document(tournament.getId());
        Map map = objectMapper.convertValue(tournament, Map.class);
        ApiFuture<WriteResult> apiFuture = tournamentRef.set(map);
        System.out.println("Tournament updated : " + apiFuture.get().getUpdateTime());
        return tournament;
    }

    public Tournament getTournament(String id) throws ExecutionException, InterruptedException {
        ApiFuture<DocumentSnapshot> tournament = firestore.collection("tournaments").document(id).get();
        System.out.println(tournament.get().getReadTime());
        return objectMapper.convertValue(tournament.get().getData(), Tournament.class);
    }

    public Tournament prepareStage(String tournamentId, StageInfo stageInfo) throws ExecutionException, InterruptedException {
        Tournament tournament = getTournament(tournamentId);
        final List<Stage> stages = Lists.newArrayList();
        Long totalGroupCount = tournament.getStages() != null ? tournament.getStages()
                .stream()
                .mapToLong(stage -> stage.getGroups().size())
                .sum() : -1;
        if (stageInfo.getPrevStageId() == null) {
            List<String> teamIds = tournament.getTeams().stream().map(Team::getId).collect(Collectors.toList());
            Stage initStage = prepareStage(stageInfo, teamIds, totalGroupCount.intValue());
            stages.add(initStage);
        } else {
            stages.addAll(tournament.getStages());
            tournament.getStages().stream().filter(stage -> stage.getId().equals(stageInfo.getPrevStageId())).findAny().ifPresent(
                    currentStage -> {
                        Stage nextStage = prepareNextStage(currentStage, stageInfo, totalGroupCount.intValue());
                        stages.add(nextStage);
                    }
            );

        }
        Tournament tournamentToUpdate = Tournament.builder()
                .id(tournament.getId())
                .teams(tournament.getTeams())
                .stages(stages)
                .name(tournament.getName())
                .initialTeamsPerGroup(tournament.getInitialTeamsPerGroup())
                .build();
        return updateTournament(tournamentToUpdate);

    }

    public List<Tournament> getAllTournaments() throws ExecutionException, InterruptedException {
        ApiFuture<QuerySnapshot> querySnapshotApiFuture = firestore.collection("tournaments").get();
        return querySnapshotApiFuture.get().getDocuments().stream()
                .map(qs -> objectMapper.convertValue(qs.getData(), Tournament.class))
                .collect(Collectors.toList());
    }


    public Stage prepareNextStage(Stage stage, StageInfo stageInfo, int totalGroupCount) {

        List<String> stageToppers = getStageToppers(stage, stageInfo.getTopNTeamsFromGroup());
        return prepareStage(stageInfo, stageToppers, totalGroupCount);

    }

    public Stage prepareStage(StageInfo stageInfo, List<String> stageToppers, int totalGroupCount) {
        final AtomicInteger counter = new AtomicInteger(totalGroupCount);

        List<List<String>> teamsByGroup = partitionTeams(stageToppers, stageInfo.getTeamsPerGroup());
        List<Group> groups = teamsByGroup.stream()
                .map(list -> buildGroup(list, stageInfo.getNumberOfSets(), stageInfo.getSetPoints(), getGroupName(counter.getAndIncrement())))
                .collect(Collectors.toList());
        return Stage.builder()
                .id(UUID.randomUUID().toString())
                .name(stageInfo.getName())
                .groups(groups)
                .build();
    }

    public Stage prepareStageWithRandomSeed(List<String> teams, int numberOfTeamsPerGroup, int numberOfSetsPerMatch) {
        List<List<String>> teamsByGroup = Lists.partition(teams, numberOfTeamsPerGroup);
        AtomicInteger counter = new AtomicInteger(0);

        List<Group> groups = teamsByGroup.stream()
                .map(list -> buildGroup(list, numberOfSetsPerMatch, 21, getGroupName(counter.incrementAndGet())))
                .collect(Collectors.toList());
        return Stage.builder()
                .groups(groups)
                .build();
    }

    public List<Team> getGroupToppers(Tournament tournament, Group group, int top) {
        return sortGroupTeamsByPoints(group, top).entrySet().stream()
                .map(entry -> tournament.getTeamsMap().get(entry.getKey()).stream().findAny())
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    public List<String> getStageToppers(Stage stage, int topNTeamsInGroup) {

        Map<String, Integer> topTeamsInStage = stage.getGroups().stream()
                .flatMap(group -> sortGroupTeamsByPoints(group, topNTeamsInGroup).entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<String, Integer> topTeamsInStageSorted = topTeamsInStage.entrySet()
                .stream()
                .sorted(comparingByValue())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));


        /*return topTeamsInStageSorted.entrySet().stream()
                .map(entry -> tournament.getTeamsMap().get(entry.getKey()).stream().findAny())
                .map(Optional::get)
                .collect(Collectors.toList());*/

        return topTeamsInStageSorted.entrySet().stream()
                .map(entry -> (entry.getKey()))
                .collect(Collectors.toList());

    }

/*    private Map<String, List<Team>> getTeamsMap(Group group) {
        return group.getTeams().stream()
                .collect(groupingBy(Team::getId));
    }*/

    private List<List<String>> partitionTeams(List<String> teams, int numberOfTeamsPerGroup) {
        int numberOfGroups = teams.size() / numberOfTeamsPerGroup;
        List<List<String>> teamsByGroup = IntStream.rangeClosed(1, numberOfGroups)
                .mapToObj(i -> Lists.<String>newArrayList())
                .collect(Collectors.toList());
        Iterator<String> iterator = teams.iterator();

        while (iterator.hasNext()) {
            teamsByGroup.forEach(group -> {
                if (iterator.hasNext()) group.add(iterator.next());
            });
        }
        return teamsByGroup;
    }

    private Group buildGroup(List<String> teams, int numberOfSetsPerMatch, int setPoints, String groupName) {
        return Group.builder()
                .name(groupName)
                .teams(teams)
                .matches(Streams.stream(new Combinator<>(teams, 2).iterator())
                        .map(combinatorList -> buildMatch(combinatorList, numberOfSetsPerMatch, setPoints))
                        .collect(Collectors.toList()))
                .build();
    }

    private Match buildMatch(List<String> teams, int numberOfSetsPerMatch, int setPoints) {
        return Match.builder()
                .id(UUID.randomUUID().toString())
                .numberOfSets(numberOfSetsPerMatch)
                .setPoints(setPoints)
                .sets(Lists.newArrayList())
                .teamOne(teams.get(0))
                .teamTwo(teams.get(1))
                .build();
    }

    private Map<String, Integer> sortGroupTeamsByPoints(Group group, int limit) {
        Map<String, Integer> teamPointsInGroup = Maps.newLinkedHashMap();
        group.getMatches().forEach(match -> match.getSets().forEach(set -> {
            teamPointsInGroup.putIfAbsent(set.getSecondPlaceId(), -set.getWinningScoreDiff());
            teamPointsInGroup.putIfAbsent(set.getWinnerId(), set.getWinningScoreDiff());
            teamPointsInGroup.computeIfPresent(set.getWinnerId(), (k, v) -> teamPointsInGroup.put(k, v + set.getWinningScoreDiff()));
            teamPointsInGroup.computeIfPresent(set.getSecondPlaceId(), (k, v) -> teamPointsInGroup.put(k, v - set.getWinningScoreDiff()));
        }));

        return teamPointsInGroup.entrySet()
                .stream()
                .sorted(reverseOrder(comparingByValue()))
                .limit(limit)
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }


    private String getGroupName(int i) {
        return i < 0 ? "" : getGroupName((i / 26) - 1) + (char) (65 + i % 26);
    }


    public Tournament updateScore(String tournamentId, Match match) throws ExecutionException, InterruptedException {

        DocumentReference tournamentRef = firestore.collection("tournaments").document(tournamentId);

        ApiFuture<Tournament> tournamentApiFuture = firestore.runTransaction(transaction -> {
            DocumentSnapshot snapshot = transaction.get(tournamentRef).get();
            Tournament tournament = objectMapper.convertValue(snapshot.getData(), Tournament.class);
            Optional<Match> matchOptional = tournament.getStages().stream()
                    .flatMap(stage -> stage.getGroups().stream())
                    .flatMap(group -> group.getMatches().stream())
                    .filter(match1 -> match.getId().equals(match1.getId()))
                    .findAny();
            matchOptional.ifPresent(match1 -> match1.setSets(match.getSets()));
            System.out.println(tournament);
            transaction.update(tournamentRef, objectMapper.convertValue(tournament, Map.class));
            return tournament;
        });
        return tournamentApiFuture.get();
    }

    public Tournament addTeam(String id, Team team) throws ExecutionException, InterruptedException {
        DocumentReference tournamentRef = firestore.collection("tournaments").document(id);

        ApiFuture<Tournament> tournamentApiFuture = firestore.runTransaction(transaction -> {
            DocumentSnapshot snapshot = transaction.get(tournamentRef).get();
            Tournament tournament = objectMapper.convertValue(snapshot.getData(), Tournament.class);
            tournament.getTeams().add(team);
            transaction.update(tournamentRef, objectMapper.convertValue(tournament, Map.class));
            return tournament;
        });
        return tournamentApiFuture.get();
    }

    public Tournament delete(String id, String teamId) throws ExecutionException, InterruptedException {
        DocumentReference tournamentRef = firestore.collection("tournaments").document(id);

        ApiFuture<Tournament> tournamentApiFuture = firestore.runTransaction(transaction -> {
            DocumentSnapshot snapshot = transaction.get(tournamentRef).get();
            Tournament tournament = objectMapper.convertValue(snapshot.getData(), Tournament.class);
            tournament.getTeams().removeIf(team -> team.getId().equals(teamId));
            transaction.update(tournamentRef, objectMapper.convertValue(tournament, Map.class));
            return tournament;
        });
        return tournamentApiFuture.get();
    }

    public Tournament updateTeam(String id, String teamId, Team team) throws ExecutionException, InterruptedException {
        DocumentReference tournamentRef = firestore.collection("tournaments").document(id);

        ApiFuture<Tournament> tournamentApiFuture = firestore.runTransaction(transaction -> {
            DocumentSnapshot snapshot = transaction.get(tournamentRef).get();
            Tournament tournament = objectMapper.convertValue(snapshot.getData(), Tournament.class);
            tournament.getTeams().removeIf(team2 -> team2.getId().equals(teamId));
            tournament.getTeams().add(team);
            transaction.update(tournamentRef, objectMapper.convertValue(tournament, Map.class));
            return tournament;
        });
        return tournamentApiFuture.get();
    }
}
