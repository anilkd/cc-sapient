package com.sathwik.badmintonservice.controller;

import com.sathwik.badmintonservice.TournamentService;
import com.sathwik.badmintonservice.model.StageInfo;
import com.sathwik.badmintonservice.model.Tournament;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/tournaments")
@Slf4j
public class TournamentController {
    private final TournamentService tournamentService;

    @Inject
    public TournamentController(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    @PostMapping
    public Tournament createTournament(@RequestBody Tournament tournament) throws ExecutionException, InterruptedException {
        log.info("saving tournament {}", tournament);
        return tournamentService.createTournament(tournament);
    }

    @PutMapping("/{id}")
    public Tournament updateTournament(@PathVariable String id, @RequestBody Tournament tournament) throws ExecutionException, InterruptedException {
        log.info("updating tournament {}", tournament);
        return tournamentService.updateTournament(tournament);
    }

    @GetMapping("/{id}")
    public Tournament getTournament(@PathVariable String id) throws ExecutionException, InterruptedException {
        log.info("getting tournament with id {}", id);
        return tournamentService.getTournament(id);
    }

    @PostMapping("/{id}/prepare")
    public Tournament prepareFirstStage(@PathVariable String id, @RequestBody StageInfo stageInfo) throws ExecutionException, InterruptedException {
        log.info("preparing tournament with id {}", id);
        return tournamentService.prepareStage(id,stageInfo);
    }

    @GetMapping
    public List<Tournament> getAllTournaments() throws ExecutionException, InterruptedException {
        log.info("getting all tournaments");
        return tournamentService.getAllTournaments();
    }
}
