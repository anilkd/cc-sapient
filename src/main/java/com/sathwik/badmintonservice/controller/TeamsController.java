package com.sathwik.badmintonservice.controller;

import com.sathwik.badmintonservice.TournamentService;
import com.sathwik.badmintonservice.model.Match;
import com.sathwik.badmintonservice.model.Team;
import com.sathwik.badmintonservice.model.Tournament;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/tournaments")
@Slf4j
public class TeamsController {
    private final TournamentService tournamentService;

    @Inject
    public TeamsController(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    @PostMapping("/{id}/teams")
    public Tournament addTeam(@PathVariable String id, @RequestBody Team team) throws ExecutionException, InterruptedException {
        log.info("adding team {}", team);
        team.setId(UUID.randomUUID().toString());
        return tournamentService.addTeam(id, team);
    }

    @PutMapping("/{id}/teams/{teamId}")
    public Tournament updateTeam(@PathVariable String id,@PathVariable String teamId, @RequestBody Team team) throws ExecutionException, InterruptedException {
        log.info("updating team {}", team);
        return tournamentService.updateTeam(id, teamId,team);
    }

    @DeleteMapping("/{id}/teams/{teamId}")
    public Tournament removeTeam(@PathVariable String id, @PathVariable String teamId) throws ExecutionException, InterruptedException {
        log.info("deleting team {}", teamId);
        return tournamentService.delete(id, teamId);
    }


}
