package com.sathwik.badmintonservice.combinators;

public interface Predicate<T> {
    Predicate NOT_NULL = new Predicate() {
        @Override
        public boolean test(Object value) {
            return value != null;
        }
    };

    boolean test(T value);

}