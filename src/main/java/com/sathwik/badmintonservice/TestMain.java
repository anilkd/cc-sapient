/*
package com.sathwik.badmintonservice;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.sathwik.badmintonservice.model.Team;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestMain {
    private static Queue<String> queue = Queues.newArrayDeque();

    public static void main(String[] args) {
        queue.add("A");
        queue.add("B");
        queue.add("C");
        queue.add("D");
        queue.add("E");
        queue.add("F");
        queue.add("G");
        queue.add("H");
        queue.add("I");
        queue.add("J");
        queue.add("K");
        queue.add("L");
        queue.add("M");
        queue.add("N");
        queue.add("O");
        queue.add("P");
        queue.add("Q");
        int numberOfTeamsInStages[] = {32, 16, 8, 2, 2};
        int numberOfTeamsPerGroupInStages[] = {4, 4, 4, 2, 2};
        int courts[] = {1, 3, 4, 2};
        LocalDateTime startTime = LocalDateTime.parse("2018-11-03 10:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        LocalDateTime endTime = LocalDateTime.parse("2018-11-03 16:30", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

        int matchAllocatedTimeInMin = 20;
        new TournamentService(firestore, objectMapper).prepareStage(4, 3, generateTeams(8))
                .getGroups().forEach(System.out::println);

    }

    private static List<Team> generateTeams(int numberOfTeamsInStage) {
        return IntStream.rangeClosed(1, numberOfTeamsInStage)
                .mapToObj(i -> Team.builder().name("Team-" + i).build())
                .collect(Collectors.toList());
    }

    static class TimeSlot {
        private LocalDateTime startTime;
        private LocalDateTime endTime;
        private int[] courts;
        private long durationOfMatchInMinutes;
        private List<LocalDateTime> courtTimes;

        public TimeSlot(LocalDateTime startTime, LocalDateTime endTime, int[] courts, long durationOfMatchInMinutes) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.courts = courts;
            this.durationOfMatchInMinutes = durationOfMatchInMinutes;
            this.courtTimes = new ArrayList<>(4);
        }

        public List<LocalDateTime> getAllAvailableSlots() {
            List<LocalDateTime> slotTimes = Lists.newArrayList();
            Duration duration = Duration.between(startTime, endTime);
            long availableDuration = duration.toMinutes();

            while (availableDuration > 0) {
                for (int court : courts) {
                    LocalDateTime matchTime = startTime.plusMinutes(durationOfMatchInMinutes);
                    slotTimes.add(matchTime);
                }
                startTime = startTime.plusMinutes(durationOfMatchInMinutes);
                availableDuration -= durationOfMatchInMinutes;
            }

            return slotTimes;
        }

    }
}
*/
