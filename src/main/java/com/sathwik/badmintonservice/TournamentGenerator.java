package com.sathwik.badmintonservice;

import com.google.common.collect.Lists;
import com.sathwik.badmintonservice.combinators.Combinator;
import com.sathwik.badmintonservice.model.*;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TournamentGenerator {


    public static Tournament buildTournament(String name, int teamCount) {
        List<Team> teams = IntStream.rangeClosed(1, teamCount)
                .mapToObj(i -> buildTeam("Team-" + i))
                .collect(Collectors.toList());

        return Tournament.builder()
                .id(UUID.randomUUID().toString())
                .name(name)
                .teams(teams)
                .initialTeamsPerGroup(4)
//                .stages(Lists.newArrayList(buildStage("Stage-01", teams)))
                .build();
    }

    private static Stage buildStage(String name, List<Team> teams) {
        List<List<Team>> teamsByGroup = Lists.partition(teams, 4);
        return Stage.builder()
                .name(name)
                .groups(Lists.newArrayList(buildGroup("A", teamsByGroup.get(0)),
                        buildGroup("B", teamsByGroup.get(1)),
                        buildGroup("C", teamsByGroup.get(2)),
                        buildGroup("D", teamsByGroup.get(3)),
                        buildGroup("E", teamsByGroup.get(4)),
                        buildGroup("F", teamsByGroup.get(5)),
                        buildGroup("G", teamsByGroup.get(6)),
                        buildGroup("H", teamsByGroup.get(7))))
                .build();
    }

    private static Group buildGroup(String name, List<Team> teams) {
        List<String> teamIds = teams.stream().map(Team::getId).collect(Collectors.toList());
        return Group.builder()
                .name(name)
                .teams(teamIds)
                .matches(buildMatches(teamIds))
                .build();
    }

    private static List<Match> buildMatches(List<String> teams) {
        List<Match> matches = Lists.newArrayList();

        new Combinator<>(teams, 2).forEach(list -> {
            Match match = Match.builder()
//                     .matchDateTime(LocalDateTime.now())
                    .court(new Random().nextInt(10))
                    .id(UUID.randomUUID().toString())
                    .numberOfSets(3)
                    .sets(buildSet(list))
                    .teamOne(list.get(0))
                    .teamTwo(list.get(1))
                    .build();
            matches.add(match);
        });

        return matches;
    }

    private static List<Set> buildSet(List<String> teams) {
        return Lists.newArrayList(Set.builder()
                .teamOnePoints(21)
                .teamTwoPoints(12)
                .teamTwoId(teams.get(0))
                .teamTwoId(teams.get(1))
                .build());
    }

    private static Set buildTeam(List<Team> list) {
        return Set.builder()
                .teamOneId(list.get(0).getId())
                .teamOnePoints(21)
                .teamTwoId(list.get(1).getId())
                .teamTwoPoints(12).build();
    }

    private static Team buildTeam(String name) {
        return Team.builder()
                .name(name)
                .id(UUID.randomUUID().toString())
                .description(UUID.randomUUID().toString())
                .players(Lists.newArrayList(buildPlayer("Player-01"),buildPlayer("Player-02")))
                .build();
    }

    private static Player buildPlayer(String name) {
        return Player.builder()
                .name(name)
                .build();
    }
}
