import * as React from "react";
import { Card, CardHeader } from "@material-ui/core";
import { Link } from "react-router-dom";
const styles = {
  root: { marginTop: 5, marginBottom: 5 },
  button: { margin: 5 },
  header: { paddingBottom: 10 },
  content: { paddingTop: 5 }
};

class TournamentItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      tournament: props.tournament
    };
  }

  render() {
    const { tournament } = this.state;
    return (
      <Card style={styles.root}>
        <Link to={"/tournaments/" + tournament.id} mode="view">
          <CardHeader title={tournament.name} style={styles.header} />
        </Link>
      </Card>
    );
  }
}

export default TournamentItem;
