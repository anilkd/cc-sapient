import * as React from "react";
import 'babel-polyfill';
import {
  Typography,
  Card,
  CardContent,
  CardHeader,
  Icon,
  Button,
  FormControl,
  InputLabel,
  Input,
  Divider
} from "@material-ui/core";
import axios from "axios";

const styles = {
  root: { marginTop: 5, marginBottom: 5 },
  button: { margin: 5 },
  header: { paddingBottom: 10 },
  content: { paddingTop: 5 }
};

class Team extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      team: props.team,
      tournamentId:props.tournamentId
    };
    this.handleEdit = this.handleEdit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  async handleEdit() {
    this.setState({ edit: true });
  }

  async handleCancel() {
    this.setState({ team: this.props.team, edit: false });
  }

  async handleSave() {
    const { team } = this.state;

    console.log("saving..."+team);

    await axios.put(`/api/tournaments/${this.state.tournamentId}/teams/${team.id}`, team);
    await this.props.refetchTournament();
  }

  async handleDelete() {
    const { team } = this.state;
    console.log("deleting...");
    await axios.delete(`/api/tournaments/${this.state.tournamentId}/teams/${team.id}`);
    console.log("deleted!");
    this.props.refetchTournament();
  }

  handleChange(e) {
    const { team } = this.state;
    const { name, value } = e.target;
    const player ={name:value}
    team.players[name]=player;
    console.log(name + " " + value);
    this.setState({ team});
  }

  render() {
    const { edit, team } = this.state;
    const { id, players } = team;

    const htmlTitleId = `team-title-${id}`;
    const title = players.map((player, index) => player.name+'\t');

    return (
      <Card style={styles.root}>
        <CardHeader
          title={
            edit
              ? team.players.map((player, index) => (
                  <FormControl fullWidth>
                    <InputLabel
                      htmlFor={htmlTitleId + player.name + (index + 1)}
                    >
                    Player { (index + 1)}
                    </InputLabel>
                    <Input
                      fullWidth
                      autoFocus
                      id={htmlTitleId + player.name + (index + 1)}
                      name={index}
                      value={player.name}
                      onChange={this.handleChange}
                      endAdornment={<Icon>edit_icon</Icon>}
                      multiline
                    />
                  </FormControl>
                ))
              : title
          }
          style={styles.header}
          action={
            edit ? (
              <div style={{ display: "flex", flexDirection: "column" }}>
                <Button
                  onClick={this.handleCancel}
                  style={styles.button}
                  color="secondary"
                  variant="fab"
                  mini
                >
                  <Icon>close_icon</Icon>
                </Button>
                <Button
                  onClick={this.handleSave}
                  style={styles.button}
                  mini
                  variant="fab"
                  color="primary"
                >
                  <Icon>check_icon</Icon>
                </Button>
              </div>
            ) : (
              <React.Fragment>
                <Button
                  onClick={this.handleEdit}
                  style={styles.button}
                  variant="fab"
                >
                  <Icon>edit_icon</Icon>
                </Button>
                <Button
                  onClick={this.handleDelete}
                  style={styles.button}
                  color="secondary"
                  variant="fab"
                >
                  <Icon>delete_icon</Icon>
                </Button>
              </React.Fragment>
            )
          }
        />
      </Card>
    );
  }
}

export default Team;
