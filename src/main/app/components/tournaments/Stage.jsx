import React from "react";
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = {
  root: { marginTop: 5, marginBottom: 5 },
  button: { margin: 5 },
  header: { paddingBottom: 10 },
  content: { paddingTop: 5 }
};


class Stage extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      stage: props.stage,
      finishedMatches: props.finishedMatches,
      totalMatches:props.totalMatches,
      completed:props.completed,
    };
  }

  render() {
    return (<div className={styles.root}>
      Finishes {this.state.finishedMatches}/{this.totalMatches} matches in this stage
            <br />
      <LinearProgress variant="determinate" value={this.state.completed} />
    </div>)
  }
}

export default Stage;