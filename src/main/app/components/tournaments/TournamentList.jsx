import * as React from "react";
import {
  Grid,
  Typography,
  CircularProgress,
  Button,
  Icon,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  InputLabel,
  Input,
  FormControl
} from "@material-ui/core";
import axios from "axios";

import TournamentItem from "./TournamentItem";

const styles = {
  formField: {
    margin: "10px 0"
  }
};
class TournamentList extends React.Component {
  constructor(props) {
    super(props);

    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleTournamentAdd = this.handleTournamentAdd.bind(this);
    this.handleChange = this.handleChange.bind(this);


    this.state = {
      openDialog: false,
      newTournament: {
        name: ""
      },
      loading: true,
      error: false

    };
  }

  async componentDidMount() {
     await this.fetchTournaments();
  }

  static getDerivedStateFromProps() {
    return null;
  }

  fetchTournaments() {
    console.log("fetching tournaments...");
    this.setState({ loading: true }, async () => {
      var config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json"
        }
      };
      axios
        .get("/api/tournaments", config)
        .then(response => {
          console.log("tournaments", response.data);
          this.setState({ tournaments: response.data, loading: false });

          console.log(response);
        })
        .catch(error => {
          console.log("error");

          console.log(error.response);
        });
    });
  }

  handleChange(e) {
    const { newTournament } = this.state;
    const { name, value } = e.target;

    this.setState({ newTournament: { [name]: value } });
    console.log(this.state.newTournament)
  }

  handleDialogOpen() {
    this.setState({ openDialog: true });
  }

  handleDialogClose() {
    this.setState({
      openDialog: false,
      newTournament: {
        name: ""
      }
    });
  }

  async handleTournamentAdd() {
    const { name } = this.state.newTournament;
    try {
      //TODO
      await axios.post("/api/tournaments", this.state.newTournament);

      this.fetchTournaments();
      this.setState({
        openDialog: false,
        newTournament: {
          name: ""
        }
      });
    } catch (e) {
      console.log("addTournament arr", e);
      this.setState({
        error: true,
        openDialog: false,
        newTournament: {
          name: "",
        }
      });
    }
  }

  render() {
    const {
      tournaments,
      loading,
      error,
      openDialog,
      newTournament
    } = this.state;
    // const { tournaments } = [...this.state.tournaments];

    if (error) return "error";

    return (
      <React.Fragment>
        <Typography variant="headline">HTA Badminton</Typography>
        <Grid container spacing={8}>
          {loading ? (
            <CircularProgress />
          ) : (
            tournaments.map(tournament => (
              <Grid key={tournament.id} item md={4} xs={12} sm={6}>
                <TournamentItem tournament={tournament} />
              </Grid>
            ))
          )}
        </Grid>
        <React.Fragment>
          <Button
            onClick={this.handleDialogOpen}
            variant="fab"
            color="primary"
            style={{ position: "fixed", right: 50, bottom: 50 }}
          >
            <Icon>add_icon</Icon>
          </Button>
          <Dialog open={openDialog} onClose={this.handleDialogClose}>
            <DialogTitle>New Tournament</DialogTitle>
            <DialogContent>
              {/* <DialogContentText>You know</DialogContentText> */}
              <FormControl style={styles.formField} fullWidth>
                <InputLabel htmlFor="add-note-title">Name</InputLabel>
                <Input
                  fullWidth
                  autoFocus
                  id="add-note-title"
                  name="name"
                  value={newTournament.name}
                  onChange={this.handleChange}
                  endAdornment={<Icon>edit_icon</Icon>}
                  multiline
                />
              </FormControl>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleDialogClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleTournamentAdd} color="primary">
                Save
              </Button>
            </DialogActions>
          </Dialog>
        </React.Fragment>
      </React.Fragment>
    );
  }
}

export default TournamentList;
