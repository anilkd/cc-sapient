import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepContent from "@material-ui/core/StepContent";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import axios from "axios";
import Stage from "./Stage";
import LinearProgress from '@material-ui/core/LinearProgress';

import {
  Grid,
  CircularProgress,
  Icon,
  Dialog,
  DialogTitle,
  DialogContent,
  FormControl,
  InputLabel,
  Input,
  DialogActions
} from "@material-ui/core";
import Team from "./Team";

const styles = theme => ({
  root: {
    width: "90%"
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  actionsContainer: {
    marginBottom: theme.spacing.unit * 2
  },
  resetContainer: {
    padding: theme.spacing.unit * 3
  }
});

class Tournament extends React.Component {
  constructor(props) {
    super(props);

    this.handleDialogOpen = this.handleDialogOpen.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleTeamAdd = this.handleTeamAdd.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.fetchTournament = this.fetchTournament.bind(this);
    this.handlePrepareStage = this.handlePrepareStage.bind(this);
    this.handlePrepareStageDialogClose = this.handlePrepareStageDialogClose.bind(
      this
    );
    this.handleNext = this.handleNext.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handlePrepareStageChange = this.handlePrepareStageChange.bind(this);
    this.fetchTournament = this.fetchTournament.bind(this);

    this.state = {
      activeStep: 0,
      openDialog: false,
      prepareStageDialog: false,
      newTeam: {
        player1: "",
        player2: ""
      },
      nextStageInfo: {
        name: "",
        teamsPerGroup: "",
        numberOfSets: "",
        setPoints: "",
        topNTeamsFromGroup: ""
      },
      loading: false,
      error: false,
      tournament: {
        teams: [],
        stages: []
      },
      totalMatches:0,
      finishedMatches:0,
      completed:0,
      currentStage:null,
      tournamentId:props.match.params.tournamentId
    };
  }

  async componentDidMount() {
  await this.fetchTournament();
    this.timer = setInterval(()=>this.progress(), 1000*20);
    // this.timer = setInterval(() => this.fetchTournament(), 1000*20);

  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  fetchTournament() {
    console.log("fetching tournament...");
    this.setState({ loading: true }, async () => {
      var config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
          "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, OPTIONS"
        }
      };
      let uri = "/api/tournaments/" + this.state.tournamentId;
      console.log(uri);
      axios
        .get(uri, config)
        .then(response => {
          console.log("tournament", response.data);
          this.setState({ tournament: response.data, loading: false, activeStep: response.data.stages.length});

          console.log(response);
        })
        .catch(error => {
          console.log("error");

          console.log(error.response);
        });
    });
  }
  handleChange(e) {
    const { newTeam } = this.state;
    const { name, value } = e.target;

    this.setState({ newTeam: { ...newTeam, [name]: value } });
  }
  handlePrepareStageChange(e) {
    const { nextStageInfo } = this.state;
    console.log(e.target);
    const { name, value } = e.target;

    this.setState({ nextStageInfo: { ...nextStageInfo, [name]: value } });
    console.log(nextStageInfo);
  }
  handleDialogOpen() {
    this.setState({ openDialog: true });
  }


  handlePrepareStageDialog() {
    this.setState({ prepareStageDialog: true });
  }

  handlePrepareStageDialogClose() {
    this.setState({ prepareStageDialog: false });
  }
  async handlePrepareStage(prevStageId) {
    const {
      name,
      teamsPerGroup,
      numberOfSets,
      setPoints,
      topNTeamsFromGroup
    } = this.state.nextStageInfo;
    const stage = {
      name,
      teamsPerGroup,
      numberOfSets,
      setPoints,
      topNTeamsFromGroup,
      prevStageId
    };
    let activeStep = this.state.activeStep + 1;
    try {
      console.log(stage);
      await axios.post(
        "/api/tournaments/" + this.state.tournamentId + "/prepare",
        stage
      );

      this.fetchTournament();
      this.setState({
        prepareStageDialog: false,
        nextStageInfo: {
          name: "",
          teamsPerGroup: "",
          numberOfSets: "",
          setPoints: "",
          topNTeamsFromGroup: ""
        },
        activeStep
      });
    } catch (e) {
      console.log("next stage err", e);
      this.setState({
        error: true,
        openDialog: false,
        newTeam: {
          player1: "",
          player1: ""
        }
      });
    }
  }
  handleDialogClose() {
    this.setState({
      openDialog: false,
      newTeam: {
        player1: "",
        player2: ""
      }
    });
  }

  async handleTeamAdd() {
    const { player1, player2 } = this.state.newTeam;
    const players = [{ name: player1 }, { name: player2 }];
    const team = { players };
    try {
      //TODO
      console.log(team);
      await axios.post(
        "/api/tournaments/" + this.state.tournament.id + "/teams",
        team
      );

      this.fetchTournament();
      this.setState({
        openDialog: false,
        newTeam: {
          player1: "",
          player2: ""
        }
      });
    } catch (e) {
      console.log("addTeam arr", e);
      this.setState({
        error: true,
        openDialog: false,
        newTeam: {
          player1: "",
          player1: ""
        }
      });
    }
  }
   handleNext(id) {
    console.log(id);
    let currentStage=id;
    this.setState({ currentStage,prepareStageDialog: true});
  }

  handleBack() {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  }

   async progress() {
     this.fetchTournament();
     console.log(this.state.activeStep)
     const stagesCount=this.state.tournament.stages.length;
     console.log(stagesCount)

    if (stagesCount>0 && this.state.activeStep == stagesCount) {
      const allMatches = this.state.tournament.stages[
        this.state.activeStep-1
      ].groups.flatMap(group => group.matches);
      console.log(allMatches)
      const totalMatches = allMatches.length;
      const finishedMatches = allMatches.filter(match => match.matchFinished)
        .length;
      const completed = (finishedMatches / totalMatches) * 100;
      this.setState({ totalMatches, finishedMatches, completed });
    }
  }
  handleReset() {
    this.setState({
      activeStep: 0
    });
  }

  render() {
    const {
      loading,
      error,
      openDialog,
      prepareStageDialog,
      newTeam
    } = this.state;
    const { player1, player2 } = newTeam;
    const { classes } = this.props;
    const { tournament } = this.state;
    let activeStep = this.state.activeStep;


    const { nextStageInfo } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} orientation="vertical">
          <Step key="teams">
            <StepLabel>Teams</StepLabel>
            <StepContent>
              <React.Fragment>
                <Grid container spacing={8}>
                  {loading ? (
                    <CircularProgress />
                  ) : (
                    tournament.teams != null &&
                    tournament.teams.map(team => (
                      <Grid key={team.id} item md={4} xs={12} sm={6}>
                        <Team
                          refetchTournament={this.fetchTournament}
                          tournamentId={this.state.tournamentId}
                          team={team}
                        />
                      </Grid>
                    ))
                  )}
                </Grid>
                <React.Fragment>
                  <Button
                    onClick={this.handleDialogOpen}
                    variant="fab"
                    color="primary"
                    style={{ position: "fixed", right: 50, bottom: 50 }}
                  >
                    <Icon>add_icon</Icon>
                  </Button>
                  <Dialog open={openDialog} onClose={this.handleDialogClose}>
                    <DialogTitle>New Team</DialogTitle>
                    <DialogContent>
                      {/* <DialogContentText>You know</DialogContentText> */}
                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-title">
                          Player 1
                        </InputLabel>
                        <Input
                          fullWidth
                          autoFocus
                          id="add-note-title"
                          name="player1"
                          value={player1}
                          onChange={this.handleChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          multiline
                        />
                      </FormControl>
                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-text">
                          Player 2
                        </InputLabel>
                        <Input
                          value={player2}
                          name="player2"
                          id="add-note-text"
                          onChange={this.handleChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          fullWidth
                          multiline
                          rowsMax={10}
                          rows={3}
                        />
                      </FormControl>
                    </DialogContent>

                    <DialogActions>
                      <Button onClick={this.handleDialogClose} color="primary">
                        Cancel
                      </Button>
                      <Button onClick={this.handleTeamAdd} color="primary">
                        Save
                      </Button>
                    </DialogActions>
                  </Dialog>
                </React.Fragment>
                <React.Fragment>
                  <Dialog
                    open={prepareStageDialog}
                    onClose={this.handlePrepareStageDialogClose}
                  >
                    <DialogTitle>Next Stage</DialogTitle>
                    <DialogContent>
                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-title">Name</InputLabel>
                        <Input
                          fullWidth
                          autoFocus
                          id="add-note-title"
                          name="name"
                          value={nextStageInfo.name}
                          onChange={this.handlePrepareStageChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          multiline
                        />
                      </FormControl>
                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-text">
                          Teams Per Group
                        </InputLabel>
                        <Input
                          value={nextStageInfo.teamsPerGroup}
                          name="teamsPerGroup"
                          id="add-note-text"
                          onChange={this.handlePrepareStageChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          fullWidth
                          multiline
                          rowsMax={1}
                          rows={1}
                        />
                      </FormControl>
                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-text">
                          Set points
                        </InputLabel>
                        <Input
                          value={nextStageInfo.setPoints}
                          name="setPoints"
                          id="add-note-text"
                          onChange={this.handlePrepareStageChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          fullWidth
                          rowsMax={1}
                          rows={1}
                        />
                      </FormControl>

                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-text">
                          Top N teams from prev stage
                        </InputLabel>
                        <Input
                          value={nextStageInfo.topNTeamsFromGroup}
                          name="topNTeamsFromGroup"
                          id="add-note-text"
                          onChange={this.handlePrepareStageChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          fullWidth
                          multiline
                          rowsMax={1}
                          rows={1}
                        />
                      </FormControl>

                      <FormControl style={styles.formField} fullWidth>
                        <InputLabel htmlFor="add-note-text">
                          Number of Sets
                        </InputLabel>
                        <Input
                          value={nextStageInfo.numberOfSets}
                          name="numberOfSets"
                          id="add-note-text"
                          onChange={this.handlePrepareStageChange}
                          endAdornment={<Icon>edit_icon</Icon>}
                          fullWidth
                          multiline
                          rowsMax={1}
                          rows={1}
                        />
                      </FormControl>
                    </DialogContent>
                    <DialogActions>
                      <Button
                        onClick={this.handlePrepareStageDialogClose}
                        color="primary"
                      >
                        Cancel
                      </Button>
                      <Button
                        onClick={() => this.handlePrepareStage(null)}
                        color="primary"
                      >
                        Save
                      </Button>
                    </DialogActions>
                  </Dialog>
                </React.Fragment>
              </React.Fragment>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={this.handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.handlePrepareStageDialog()}
                    className={classes.button}
                  >
                    Prepare
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
          {tournament.stages.map((stage, index) => {
            return (
              <Step key={stage.id}>
                <StepLabel>{stage.name}</StepLabel>
                <StepContent>
                  <div className={styles.root}>
                        Finished {this.state.finishedMatches}/{this.state.totalMatches} matches in this stage
                              <br />
                        <LinearProgress variant="determinate" value={this.state.completed} />
                  </div>
                  <div className={classes.actionsContainer}>
                    <div>
                      <Button
                        //disabled={activeStep === 0}
                        onClick={this.handleBack}
                        className={classes.button}
                      >
                        Back
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => this.handleNext(stage.id)}
                        className={classes.button}
                      >
                        {1 === this.state.totalMatches.length
                          ? "Finish"
                          : "Next"}
                      </Button>
                    </div>
                  </div>
                  <React.Fragment>
                                    <Dialog
                                      open={prepareStageDialog}
                                      onClose={this.handlePrepareStageDialogClose}
                                    >
                                      <DialogTitle>Next Stage</DialogTitle>
                                      <DialogContent>
                                        <FormControl style={styles.formField} fullWidth>
                                          <InputLabel htmlFor="add-note-title">Name</InputLabel>
                                          <Input
                                            fullWidth
                                            autoFocus
                                            id="add-note-title"
                                            name="name"
                                            value={nextStageInfo.name}
                                            onChange={this.handlePrepareStageChange}
                                            endAdornment={<Icon>edit_icon</Icon>}
                                            multiline
                                          />
                                        </FormControl>
                                        <FormControl style={styles.formField} fullWidth>
                                          <InputLabel htmlFor="add-note-text">
                                            Teams Per Group
                                          </InputLabel>
                                          <Input
                                            value={nextStageInfo.teamsPerGroup}
                                            name="teamsPerGroup"
                                            id="add-note-text"
                                            onChange={this.handlePrepareStageChange}
                                            endAdornment={<Icon>edit_icon</Icon>}
                                            fullWidth
                                            multiline
                                            rowsMax={1}
                                            rows={1}
                                          />
                                        </FormControl>
                                        <FormControl style={styles.formField} fullWidth>
                                          <InputLabel htmlFor="add-note-text">
                                            Set points
                                          </InputLabel>
                                          <Input
                                            value={nextStageInfo.setPoints}
                                            name="setPoints"
                                            id="add-note-text"
                                            onChange={this.handlePrepareStageChange}
                                            endAdornment={<Icon>edit_icon</Icon>}
                                            fullWidth
                                            rowsMax={1}
                                            rows={1}
                                          />
                                        </FormControl>

                                        <FormControl style={styles.formField} fullWidth>
                                          <InputLabel htmlFor="add-note-text">
                                            Top N teams from prev stage
                                          </InputLabel>
                                          <Input
                                            value={nextStageInfo.topNTeamsFromGroup}
                                            name="topNTeamsFromGroup"
                                            id="add-note-text"
                                            onChange={this.handlePrepareStageChange}
                                            endAdornment={<Icon>edit_icon</Icon>}
                                            fullWidth
                                            multiline
                                            rowsMax={1}
                                            rows={1}
                                          />
                                        </FormControl>

                                        <FormControl style={styles.formField} fullWidth>
                                          <InputLabel htmlFor="add-note-text">
                                            Number of Sets
                                          </InputLabel>
                                          <Input
                                            value={nextStageInfo.numberOfSets}
                                            name="numberOfSets"
                                            id="add-note-text"
                                            onChange={this.handlePrepareStageChange}
                                            endAdornment={<Icon>edit_icon</Icon>}
                                            fullWidth
                                            multiline
                                            rowsMax={1}
                                            rows={1}
                                          />
                                        </FormControl>
                                      </DialogContent>
                                      <DialogActions>
                                        <Button
                                          onClick={this.handlePrepareStageDialogClose}
                                          color="primary"
                                        >
                                          Cancel
                                        </Button>
                                        <Button
                                          onClick={() => this.handlePrepareStage(stage.id)}
                                          color="primary"
                                        >
                                          Save
                                        </Button>
                                      </DialogActions>
                                    </Dialog>
                                  </React.Fragment>
                </StepContent>
              </Step>
            );
          })}
        </Stepper>

      </div>
    );
  }
}

Tournament.propTypes = {
  classes: PropTypes.object
};

export default withStyles(styles)(Tournament);
