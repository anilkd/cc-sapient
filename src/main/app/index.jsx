'use strict';

import React from "react";
import ReactDOM from "react-dom";
import {
    AppBar, createMuiTheme, Drawer, Grid, IconButton, List, ListItem, ListItemText, ListSubheader, MuiThemeProvider,
    Toolbar,
    Typography
} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {HashRouter, Link, Route, Switch} from "react-router-dom";
import CreditCardPage from "./pages/CreditCardPage.jsx";


const theme = createMuiTheme({
    palette: {
        type: 'light'
    },
});

class MenuLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };

        this.menuOpen = this.menuOpen.bind(this);
        this.menuClose = this.menuClose.bind(this);
    }

    menuOpen() {
        this.setState({open: true});
    };

    menuClose() {
        this.setState({open: false});
    };

    render() {
        return (
            <AppBar position="static">
                <Toolbar>
                    <IconButton aria-label="Menu" onClick={this.menuOpen}>
                        <MenuIcon/>
                    </IconButton>
                    <Typography variant="title" color="inherit" style={{paddingLeft: "16px", color: "white"}}>
                        Credit Card System
                    </Typography>
                </Toolbar>

                <Drawer open={this.state.open} onClose={this.menuClose}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.menuClose}
                    >
                        <List subheader={<ListSubheader>Pages</ListSubheader>}>
                            <ListItem button component={Link} to="/">
                                <ListItemText primary="Credit Card System"/>
                            </ListItem>

                        </List>
                    </div>
                </Drawer>
            </AppBar>
        );
    }
}

const Main = () => (
    <Grid container justify="center">
        <Grid item xs={12} style={{padding: "16px"}}>
            <div style={{margin: '16px'}}>
                <Switch>

                    <Route exact path='/' component={CreditCardPage}/>
                    <Route exact path='/creditcards' component={CreditCardPage}/>
                </Switch>
            </div>
        </Grid>
    </Grid>
);

const App = () => (
    <div>
        <MenuLayout/>
        <Main/>
    </div>
);

ReactDOM.render(
    <HashRouter>
        <MuiThemeProvider theme={theme}>
            <App/>
        </MuiThemeProvider>
    </HashRouter>,
    document.getElementById('application')
);