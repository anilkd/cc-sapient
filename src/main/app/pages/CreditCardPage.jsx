import React from "react";
import Button from "@material-ui/core/Button";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import axios from "axios";
import "@babel/polyfill";
import ErrorComponentWrapper from "../components/ErrorComponent.jsx";
import CardList from "../components/CardListComponent.jsx";



export default class CreditCardListComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formData: {
        name: "",
        creditCardNumber: "",
        limit: {
          "amount": "0.00",
          "currency": "GBP"
        },

      },
      submitted: false,
      displayMessage: false,
      variant: "success",
      message: '',
      cards: props.cards
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchCards = this.fetchCards.bind(this);
  }

  handleClose(event, reason) {
    if (reason == "clickaway") {
      return;
    }
    this.setState({ displayMessage: false });
  };

  async handleChange(event) {
    const { formData } = this.state;
    if (event.target.name === 'limit') {
      formData[event.target.name].amount = event.target.value;
    } else {
      formData[event.target.name] = event.target.value;
    }
    this.setState({ formData });
  }
  async componentDidMount() {
    await this.fetchCards();

  }
  async fetchCards() {
    this.setState({ loading: true }, async () => {
      var config = {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/json",
          "Access-Control-Allow-Methods": "GET, PUT, POST, DELETE, OPTIONS"
        }
      };
      let uri = "/api/cards/";
      axios
        .get(uri, config)
        .then(response => {
          console.log("cards", response.data);
          this.setState({ cards: response.data, loading: false });
        })
        .catch(error => {
          this.setState({ loading: false, displayMessage: true, variant: 'error', message: 'Error loading cards' });
        });
    });
  }
  async handleSubmit() {
    const { formData } = this.state;
    this.setState({ submitted: true }, () => {
      setTimeout(() => this.setState({ submitted: false }), 5000);
    });

    this.setState({ loading: true }, async () => {
      let uri = "/api/cards/";
      console.log(formData);
      await axios.post(uri, formData)
        .then(response => {
          this.setState({
            loading: false,
            displayMessage: true,
            variant: 'success',
            message: 'Added new Card successfully',
            formData: {
              name: "",
              creditCardNumber: "",
              limit: {
                "amount": "0.00",
                "currency": "GBP"
              },

            }
          });
          this.fetchCards();

        })
        .catch(error => {
          console.log(error.response);
          this.setState({ loading: false, displayMessage: true, variant: 'error', message: error.response.data.errors[0].defaultMessage });

        });

    });
  }

  render() {
    const { formData, submitted, variant, displayMessage, message, cards } = this.state;
    return (
      <div>
        {displayMessage == true && <ErrorComponentWrapper
          open={displayMessage}
          onClose={this.handleClose}
          variant={variant}
          message={message}
        />
        }
        <ValidatorForm ref="form" onSubmit={this.handleSubmit}>
          <h2>Credit Card System</h2>
          <h4>Add</h4>
          <TextValidator
            label="Name"
            onChange={this.handleChange}
            name="name"
            value={formData.name}
            validators={["required"]}
            errorMessages={["Please enter Name"]}
          />
          <br />
          <TextValidator
            label="Card Number"
            onChange={this.handleChange}
            name="creditCardNumber"
            value={formData.creditCardNumber}
            validators={["required", "matchRegexp:^[0-9]{12,19}$"]}
            errorMessages={["Invalid Card"]}
          />
          <br />
          <TextValidator
            label="Limit"
            onChange={this.handleChange}
            name="limit"
            value={formData.limit.amount}
            validators={[
              "required",
              "minNumber:0",
              "maxNumber:1000000",
              "matchRegexp:^\\d+(\\.\\d{1,2})?$"
            ]}
            errorMessages={["Invalid card limit"]}
          />
          <br />
          <br />
          <Button raised type="submit" disabled={submitted}>
            {(submitted && "Your form is submitted!") || (!submitted && "Submit")}
          </Button>
        </ValidatorForm>
        <br />
        <br />
        {cards != null && <CardList cards={cards} />}

      </div>
    );
  }
}
